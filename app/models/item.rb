require "json"
require "pp"
require "open-uri"
require "net/http"
class Item < ActiveRecord::Base
  def body_lead(length = 200)
    return "" if body.nil?
    return body if body.length < length
    result  = body.split(/(\r\n|\n|\r)/).delete_if{|s| s.strip.blank?}[0,5].join("\n")
    return result +"\n（略）" if result.length < length
    result.split(//)[0,100].join + "（略）"
  end

  class << self

    # 自分のユーザーのフォロー中の相手
    def get_followees(user_id)
      url = "https://qiita.com/api/v2/users/#{user_id}/followees"
      target_users = get_qiita_api(url).map{|user| user["id"]}
    end

    # 自分と自分がフォローしている相手
    def target_users(user_id)
      followees = get_followees(user_id)
      followees << user_id.to_s
    end

    # user_id の1ページ目にあたる記事を取得
    def get_items(user_id)
      url = "https://qiita.com/api/v2/users/#{user_id}/items"
      items = get_qiita_api(url)
      items.each do | item |
        record = self.find_or_initialize_by(qiita_id: item["id"])
        [:title, :url, :body, :rendered_body].each do | column |
          record.send("#{column}=", item[column.to_s])
        end
        record.user_id = item["user"] && item["user"]["id"]
        record.user = item["user"] && item["user"]["name"]
        record.post_at = item["created_at"] || Time.now
        record.save
      end
    end

    # qiitaからuser_idとそのフォロワーの投稿を取得
    def get_qiita
      users = target_users(ENV["USER_ID"])
      users.each do | user |
        get_items(user)
        sleep 1
      end
      send_chatwork
    end

    def send_chatwork
      records = Item.where(chatwork_send: nil).order("post_at")
      return if records.count == 0
      message = ""
      records.each do | item |
        message += "[info][title]#{item.title} - #{item.user_id} (#{item.post_at.strftime("%m/%d %H:%M")})[/title]\n#{item.body_lead(800)}\nURL #{item.url}\n[/info]\n"
      end
      return if message.blank?
      post_chatwork_api(message)
      records.each do | item |
        item.chatwork_send = true
        item.save
      end
      message
    end

    # qiita APIから取得
    def get_qiita_api(url)
      uri = URI.parse(url)
      puts url
      request = Net::HTTP::Get.new(uri.request_uri)
      request["Authorization"] = "Bearer #{ENV["QIITA_TOKEN"]}"
      response = Net::HTTP.start(uri.host, uri.port, {:use_ssl => true, :verify_mode => OpenSSL::SSL::VERIFY_NONE}){|http|
        http.request(request)
      }
      return JSON.parse(response.body)
    rescue => e
      puts e
      []
    end

    # ROOMIDに送信
    ROOM_ID = ENV["ROOM_ID"]
    def post_chatwork_api(message)
      url = "https://api.chatwork.com/v2/rooms/#{ROOM_ID}/messages"
      uri = URI.parse(url)
      https = Net::HTTP.new(uri.host, uri.port)
      https.use_ssl = true # HTTPSでよろしく
      request = Net::HTTP::Post.new(uri.request_uri)
      request.add_field "X-ChatWorkToken", ENV["API_KEY"]
      request.set_form_data :body => message
      https.request(request)
    end
  end
end
