class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :qiita_id
      t.string :user_id
      t.string :title
      t.string :url
      t.string :user
      t.text :body
      t.text :rendered_body
      t.boolean :chatwork_send
      t.timestamp :post_at
      t.timestamps null: false
    end
  end
end
